import React, { useState, useEffect } from 'react';
import { Router, Switch, Route, NavLink } from 'react-router-dom';
import axios from 'axios';

import Login from './pages/Login';
import Product from './pages/Product';
import Wishlist from './pages/Wishlist';
import './App.css'
import PrivateRoute from './routes/PrivateRoute';
import PublicRoute from './routes/PublicRoute';
import { getToken, removeUserSession, setUserSession, getUser } from './utils/common';
import PrivateLayout from './layout/PrivateLayout';
import { history } from './utils/history' ;
function App() {
  const [authLoading, setAuthLoading] = useState(true);
  /*const user = getUser();
  console.log(user._id);*/
  useEffect(() => {
    const token = getToken();
    if (!token) {
      return;
    }

    /* axios.get(`http://localhost:/verifyToken?token=${token}`).then(response => {
       setUserSession(response.data.token, response.data.user);
       setAuthLoading(false);
     }).catch(error => {
       removeUserSession();
       setAuthLoading(false);
     });*/
  }, []);

  /*if (authLoading && getToken()) {
    return <div className="content">Checking Authentication...</div>
  }*/

  return (
    <Router history={ history }>
      <Switch>
        <PrivateRoute exact path="/" component={() => (
          <PrivateLayout route="/" name="wishlist">
            <Wishlist />
          </PrivateLayout >
        )} />
        <PublicRoute path="/login" component={Login} />
        <PrivateRoute path="/product" component={() => (
          <PrivateLayout route="/product" name="product">
            <Product />
          </PrivateLayout >
        )} />
      </Switch>

    </Router>

  );
}

export default App;