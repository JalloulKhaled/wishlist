import React from 'react';
import { Grid } from "@material-ui/core";
import TabBarComponent from "../components/TabBarComponent"
function PrivateLayout({ children, route, name }) {

   
    return (
        <Grid container direction="column">
            <Grid item md={12}>
                <TabBarComponent route={route} />
            </Grid>
            <Grid item md={12}>
                {children}
            </Grid>
        </Grid>
    );
}

export default PrivateLayout;