import React from 'react';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { AppBar, FormControl , InputLabel,NativeSelect, MenuItem, Menu, IconButton, Toolbar, Typography, Button, List, ListItem, ListItemText } from '@material-ui/core';
import { history } from '../utils/history';
import { Util } from 'reactstrap';
import { getUser, removeUserSession } from '../utils/common';
import { makeStyles } from '@material-ui/core/styles';
function TabBarComponent({ route }) {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    button: {
      marginLeft: 100,
    },
    title: {
      flexGrow: 1,
    },
    accountStyle: {
      marginLeft: 'auto',
    },
    appBarStyle: {
      height: '70px',
    },
    menuStyle: {
      display: 'flex',
      alignItems: 'center',
    },
    menuItem: {
     minWidth: '100px',
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 50,
      color : 'white',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    
    
  }));
  const classes = useStyles();

  const changePage = (prop) => (event) => {
    history.push(prop);
  };
  const handleLogout = () => {
    removeUserSession();
    history.push('/login');
  }
  //profile menu
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };
  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const isMenuOpen = Boolean(anchorEl);
  const menuId = 'primary-search-account-menu';
  const open = Boolean(anchorEl);

  //* */
  return (
    <AppBar className={classes.appBarStyle} position="static">
      <Toolbar>

        <List component="nav" className={classes.menuStyle}>
          <ListItem button selected={route === "/"} onClick={changePage("/")}>
            <ListItemText className={classes.menuItem}>
              <Typography color="inherit" variant="inherit">
                My Wishlists
               </Typography>
            </ListItemText>
          </ListItem >
          <ListItem  button selected={route === "/product"} onClick={changePage("/product")}>
            <ListItemText className={classes.menuItem}>
              <Typography color="inherit" variant="inherit">
                My products
               </Typography>
            </ListItemText>
            
          </ListItem >
          
        </List>
        
        <MenuItem  className={classes.accountStyle} onClick={handleProfileMenuOpen}>
          <IconButton
            aria-label="account of current user"
            aria-controls="primary-search-account-menu"
            aria-haspopup="true"
            color="inherit"
          >
            <AccountCircle />
            
          </IconButton>
          <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={()=>handleLogout()}>LogOut</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
              </Menu>
        </MenuItem>
        <FormControl className={classes.formControl}>
        <NativeSelect 
          defaultValue={10}
          inputProps={{
            name: 'name',
            id: 'uncontrolled-native',
          }}
        >
          <option value={10}>TND</option>
          <option value={20}>EUR</option>
          <option value={30}>USD</option>
        </NativeSelect>
      </FormControl>
      </Toolbar>
    </AppBar>
  );
}

export default TabBarComponent;