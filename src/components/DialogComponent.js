import React from 'react';
import { DialogTitle, IconButton } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CancelIcon from '@material-ui/icons/Cancel';


/**
 * Component global FormDialog
 *
 * @component
 *
 * @example
 * return (
 *   <FormDialog/>
 * )
 */
function DialogComponent({
  openFormDialog,
  handleCloseFormDialog = false,
  title,
  children,
}) {


  return (
    <div>
      <Dialog
        open={openFormDialog}
        onClose={handleCloseFormDialog}
      >
        <DialogTitle>
          <span>{(title)}</span>
          <IconButton
            onClick={handleCloseFormDialog}
          >
            <CancelIcon  />
          </IconButton>
        </DialogTitle>
        <DialogContent >
          {children}
        </DialogContent>
      </Dialog>
    </div>
  );
}
export default DialogComponent ;