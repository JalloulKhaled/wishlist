import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from '../utils/common';
import '../assets/css/login.css'


function Login(props) {
  const [loading, setLoading] = useState(false);
  const email = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.post('http://localhost:3000/api/signin',{ email: email.value, password: password.value }).then(response => {
     setLoading(false);
      console.log(response);
    setUserSession(response.data.token, response.data.message);
    props.history.push('/');
    }).catch(error => {
        setError("Something went wrong. Please try again later.");
      setLoading(false);
      console.log(error.response.status);
     if (error.response.status === 404) setError("Check Credentials!");
      else setError("Something went wrong. Please try again later.");
    });
  }

  return (
    <div className="parent">
      <h2 className="label">SignIn</h2><br /><br />
      <div className="child" style={{ marginTop: 5 }}>
        Username<br />
        <input style={{ width: '65%' }} type="text" {...email} autoComplete="new-password" />
      </div>
      <div className="child">
        Password<br />
        <input style={{ width: '65%' }}type="password" {...password} autoComplete="new-password" />
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" className="btnLogin" value={loading ? 'Loading...' : 'SignIn'} onClick={handleLogin} disabled={loading} /><br />
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Login;