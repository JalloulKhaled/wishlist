import React,{ useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Button,DialogActions,FormControl,NativeSelect } from '@material-ui/core';
import DialogComponent from '../components/DialogComponent';
import { getUser,useFormInput } from '../utils/common';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 0,
  },
  paper1: {
    height: 900,
    width: '100%',
  },
  paper2: {
    height: 800,
    width: '100%',
    paddingTop: '5%',
  },
  grid1: {
    height: 800,
    width: '20%',
    marginTop: 0,

  },
  grid2: {
    height: 800,
    width: '79%',
    marginTop: 0,
  },
  control: {
    padding: theme.spacing(0),
  },
  btn: {
    border: '2px solid blue',
    color: 'blue',
    width: '90%',
    marginLeft: '5%',
    marginTop:5,
  },
  firstLine: {
    color: 'blue',
    width: '500px',
    marginLeft: '5%',
    marginTop:5,
  },
  formControl:{
    width: '100%',
    marginTop:'20px',
  },
  form2:{
    width: '60%',
    marginLeft:'20%',
    height: '50%',
    border: '1px solid black',
  },
  btnStyle:{
    width: '10%',
    height: '25px',
    marginTop:'15px', 
    border: '1px solid blue',
    borderRadius:4,
  }
}));

export default function Product() {
  const classes = useStyles();
  const [dialogValue, setDialogValue] = useState(false);
  const handleCloseDialog=()=>{ setDialogValue(false) };
  const handleOpenDialog=()=>{ setDialogValue(true) };
  const productName = useFormInput('');
  const productPrice = useFormInput('');
  const productDesc = useFormInput('');



  return (
    <Grid container className={classes.root} spacing={0}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={0}>
        
            <Grid key={0} className={classes.grid1} item>
              <Paper className={classes.paper1} >
                 <div>
                    <Button className={classes.btn} onClick ={()=>handleOpenDialog()}>+Add Prodcut</Button>
                </div>
              </Paper>
            </Grid>
            <Grid className={classes.grid2} key={1} item>
              <Paper className={classes.paper2} 
              >
               <h2 style={{ marginLeft:"40%" }}>Add product</h2>
          <Grid container className={classes.form2} spacing={3}>
            <Grid item xs={4}>
                Product Name<br />
                 <input type="text" style={{ height:"30px" }} {...productName} autoComplete="new-password" />
            </Grid>
            <Grid item xs={4} sm={4}>
                Price<br />
                 <input type="text" style={{ height:"30px" }}{...productPrice} autoComplete="new-password" />
            </Grid>
            <Grid item xs={3} sm={3}>
             <FormControl className={classes.formControl}>
              <NativeSelect 
                  defaultValue={10}
                  inputProps={{
                    name: 'name',
                    id: 'uncontrolled-native',
                  }}
             >
                  <option value={10}>TND</option>
                  <option value={20}>EUR</option>
                  <option value={30}>USD</option>
              </NativeSelect>
            </FormControl>
           </Grid>
           <Grid item xs={12} sm={12}>
               Price<br />
                <textarea style={{ width: '100%',height:'50px' }}type="text" {...productDesc} autoComplete="new-password" />
           </Grid>
           <Grid item xs={6} sm={6}>
             <FormControl className={classes.formControl}>
               <NativeSelect 
                  defaultValue={10}
                  inputProps={{
                    name: 'name',
                    id: 'uncontrolled-native',
                  }}
                >
                  <option value={10}>Wishlist1</option>
                  <option value={20}>Wishlist2</option>
                  <option value={30}>Wishlist3</option>
                </NativeSelect>
             </FormControl>
          </Grid>
          <Grid item xs={6} sm={6}>
             <FormControl className={classes.formControl}>
                <NativeSelect 
                  defaultValue={10}
                  inputProps={{
                    name: 'name',
                    id: 'uncontrolled-native',
                  }}
                >
                  <option value={10}>To Buy</option>
                  <option value={20}>Bought</option>
                </NativeSelect>
            </FormControl>
          </Grid>
            <Button style={{ marginLeft:'70%',color:'blue' }} className={classes.btnStyle} onClick={handleCloseDialog} color="primary">
                Cancel
            </Button>
            <Button style={{ marginLeft:'3%',color:'white',background:'blue'}} className={classes.btnStyle}/*onClick={() =>{addWishlist(productName.value)}} */color="primary">
                 Save
            </Button>
        
      </Grid>

              </Paper>
            </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}