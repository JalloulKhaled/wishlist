import React,{ useState , useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Button,DialogActions,Typography,List,ListItem,ListItemText } from '@material-ui/core';
import DialogComponent from '../components/DialogComponent'
import { getUser } from '../utils/common';
import { useFormInput } from '../utils/common';
import { addWishlist,methodsServices } from '../service/wishlistService';
//handle wishlistname input

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper1: {
    height: 800,
    width: '100%',
  },
  paper2: {
    height: 800,
    width: '100%',
  },
  grid1: {
    height: 800,
    width: '20%',
  },
  grid2: {
    height: 800,
    width: '79%',
  },
  control: {
    padding: theme.spacing(0),
  },
  btn: {
    border: '2px solid blue',
    color: 'blue',
    width: '90%',
    marginLeft: '5%',
    marginTop:5,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
    textAlign: 'center',
  },
  listW: {
    width: '100%',
  },
}));


//populate list of wishlit
function generate(element) {
  return [0, 1, 2].map((value) =>
    React.cloneElement(element, {
      key: value,
    }),
  );
}

export default function Wishlists() {
  //get Wishlist Service

  
  const [list, setList] = useState([]);
  useEffect(() => {
    methodsServices.getAllWhislist().then((result)=>{
      console.log(result);
      if (result && result.length>0) 
      setList(result);})
  }, []);
  const classes = useStyles();
  const wishName = useFormInput('');
  const [dialogValue, setDialogValue] = useState(false);
  const handleCloseDialog=()=>{ setDialogValue(false) };
  const handleOpenDialog=()=>{ setDialogValue(true) };


  return (
    <Grid container className={classes.root} spacing={0}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={0}>
        
          <Grid key={0} className={classes.grid1} item>
              <Paper className={classes.paper1} >
              <div>
    <Button className={classes.btn} onClick ={()=>handleOpenDialog()}>+Add Wishlist</Button>
    <DialogComponent
          openFormDialog={dialogValue}
          handleCloseFormDialog={handleCloseDialog}
          buttonTitle='title'
          title='Add wishlist'
        >
  <div>
        Wishlist name<br />
        <input type="text" {...wishName} autoComplete="new-password" />
      </div>
          <DialogActions>
          <Button onClick={handleCloseDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={() => {addWishlist(wishName.value)}} color="primary">
            Add
          </Button>
        </DialogActions>
        </DialogComponent>
  </div>
  
    
     
        <Grid item xs={12} md={6}>
          
          <div >
            <List className={classes.listW}>
              {list.map((item, index) => (
                <ListItem key={'item-'+index}>
                  <ListItemText className={classes.demo}
                    primary={item.wishName}
                  />
                </ListItem>
              ))}
            </List>
          </div>
        </Grid>
  
  </Paper>
            </Grid>
            <Grid className={classes.grid2} key={1} item>
              <Paper className={classes.paper2} />
            </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}