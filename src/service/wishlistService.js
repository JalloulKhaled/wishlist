import React from 'react'
import { getUser } from '../utils/common';
import axios from 'axios';
import config from '../config/config'

export const methodsServices = {
    getAllWhislist
   };
async function getAllWhislist(){
   
    const user = getUser();
    let result  = await  axios.post(config.baseUrl+'/getAll',{
      user: user._id
    },{"headers": {
  
      "content-type": "application/json",
      
    }  }).then(response => {
    console.log(response.data);
      return Promise.resolve(response.data);
    
    });
   return result 
  }

  //Add Wishlist Service
export const addWishlist = (email) => {
    
    const user = getUser();
    //setError(null);
    axios.post(config.baseUrl+'/addwish',{ wishName: email, user: user._id }).then(response => {
    //props.history.push('/');
    }).catch(error => {
        console.log("Something went wrong. Please try again later.");
     // console.log(error.response);
     if (error.response === 404) console.log("Check Credentials!");
      else console.log("Something went wrong. Please try again later.");
    });
    alert("Wish list added for you ");
  
  }